<?php
	
	require_once 'model/points.php';

	if( !isset( $_POST ) ){
		exit( "FORBIDDEN" );
	}

	exit( json_encode( getScore( $_POST['points'] ) ) );

?>
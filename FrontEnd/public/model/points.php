<?php

function getScore( $numbers ){

	$points = Array(
		"Pair" => 10,
		"Triple" => 25,
		"Poker" => 40,
		"Full-house" => 50,
		"Sequence" => 60,
		"Yam" => 100,
	);

	$numbers_out = Array(  );
	foreach ($numbers as $number) {
		$numbers_out[ $number ]++;
	}

	arsort( $numbers_out );
	$number_out_size = count( $numbers_out );

	switch ( $number_out_size ) {
		case 1:
			return Array( "play" => "Yam", "score" => $points['Yam'] );
		
		case 2:
		case 3:

			$context_score = Array();
			$triple_out = false;
			foreach ($numbers_out as $number => $times_out ) {

				if( $times_out == 4 ){
					$context_score[] = Array( 'play' => 'Poker', 'score' => $points['Poker'] );
					break;
				}else if( $times_out == 3 ){
					$triple_out = true;
					$context_score[] = Array( 'play' => 'Triple', 'score' => $points['Triple'] );
				}else if( $times_out == 2 ){
					if( $triple_out ){
						$context_score = Array();
						$context_score[] = Array( 'play' => 'Full-house', 'score' => $points['Full-house'] );
					}else{
						$context_score[] = Array( 'play' => 'Pair', 'score' => $points['Pair'] );
					}
					
				}

			}
			
			foreach( $context_score as $score ){
				$context_score_final[ $score['play'] ] += $score['score'];
			}

			return Array( "play" => implode(", ", array_keys( $context_score_final ) ), "score" => array_sum( $context_score_final ) );

		case 4:
			return Array( "play" => "Pair", "score" => $points['Pair'] );

		case 5:
			return Array( "play" => "Sequence", "score" => $points['Sequence'] );

	}

}

?>
<?php

require_once './vendor/autoload.php';

$loader = new Twig_Loader_Filesystem('./templates');
$twig = new Twig_Environment($loader, array(
	'cache' => false,
));


$configuration = Array();
$configuration['allowed_rounds'] = 13;
$configuration['allowed_attempts'] = 3;
$configuration['dices_blocked'] = 4;
$configuration['dice_number'] = 5;

echo $twig->render('index.html', $configuration);

?>
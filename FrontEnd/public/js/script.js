var allowedAttempts = 0;
var allowedRounds = 0;
var maxDicesBlocked = 0;
var numberOfDices = 0;
var firstPlay = 0;
var newScore = "";
var newPlay = "";

var firstPlayerReady = 0;
var secondPlayerReady = 0;

var rounds = 1;
var attempts = 0;

var player1points = 0;
var player2points = 0;

var currentPlay = "";
var currentRound = 1;
var currentRoundCount = 0;

var color_dices_player1 = "";
var color_dices_player2 = "";
var currentPlayer = 1;

var firstRound = true;

var dices_faces_numbers = {
    1: 1,
    2: 6,
    3: 4,
    4: 3,
    5: 2,
    6: 5,
};

var players_total_score = {1: 0, 2: 0};

var soundEnabled = true;

var selected_difficulty = 1;

//setUpVariables(3, 13, 3, 5);

function setUpVariables(allowedAttemptsVal, allowedRoundsVal, maxDicesBlockedVal, numberOfDicesVal) {
    allowedAttempts = allowedAttemptsVal;
    allowedRounds = allowedRoundsVal;
    maxDicesBlocked = maxDicesBlockedVal;
    numberOfDices = numberOfDicesVal;
}

function startPlaying() {
    $("#start-container").fadeOut("fast", function () {
        $("#playing-container").fadeIn("fast");
    });
    $("#player1").addClass("playing");
    $(".play-controls").css({ display: "table" });
    $(".play-screen").css({ display: "table-cell" });
}

function validatePlayer1(event) {
    var name = $("#player1-first-name").val();
    var surname = $("#player1-last-name").val();
    if (validatePlayer(name, surname, 1) == 1) {
        $("#player1-first-name").addClass("disabled");
        $("#player1-last-name").addClass("disabled");
        $("#submitPlayer1").hide();
        $('#form-player1 .dice_color_options').addClass("selected");
        
        firstPlayerReady = 1;
        if (firstPlayerReady == 1 && secondPlayerReady == 1) {
            startPlaying();
        }
    }
}

function validatePlayer2(event) {
    var name = $("#player2-first-name").val();
    var surname = $("#player2-last-name").val();
    if (validatePlayer(name, surname, 2) == 1) {
        $("#player2-first-name").addClass("disabled");
        $("#player2-last-name").addClass("disabled");
        $('#form-player2 .dice_color_options').addClass("selected");
        $("#submitPlayer2").hide();
        secondPlayerReady = 1;
        if (firstPlayerReady == 1 && secondPlayerReady == 1) {
            startPlaying();
        }
    }
}

function validatePlayer(name, surname, player) {
    var dice_color = getSelectedColor(player);
    console.log(dice_color);
    if(player==1){
        color_dices_player1 = dice_color;
    }else{
        color_dices_player2 = dice_color;
    }

    if (name.trim() != "" && surname.trim() != "" && dice_color != "none") {
        var words_name = name.trim().split(/\s+/);
        var num_words_name = words_name.length;
        var words_surname = surname.trim().split(/\s+/);
        var num_words_surname = words_surname.length;
        if (num_words_name == 1 && num_words_name == 1) {
            setUpPlayer(name.trim(), surname.trim(), player);
            return 1;
        } else {
            alert("Insert only one word per input field");
            return 0;
        }
    } else {
        alert("You need to insert s name, surname and select a color!");
        return 0;
    }
}

function setUpPlayer(name, surname, player) {
    var firstInitial = name.substr(0, 1);
    var secondInitial = surname.substr(0, 1);
    $("#player" + player)
        .find(".player-logo")
        .find("p")
        .html(firstInitial + secondInitial);
    $("#player" + player)
        .find(".name")
        .html(name + " " + surname);
}

function setUpAttempts() {
    var attempts = "<ul class='attempts'><ul>";

    $(".player.playing .current-round > ul > li.current").append(attempts);

    for (i = 1; i <= allowedAttempts; i++) {
        var attempt = "<li class='attempt" + i + "'></li>";

        $(".player.playing .current-round > ul > li.current .attempts").append(
            attempt
        );
    }
}

var previous;

function randomizeNumber() {
    //Randimizes a number between 1 and 6
    var random = Math.floor(Math.random() * 6 + 1);
    return random;
}

function rollDice(side, dice) {
    //Removes old class and adds the new

    var currentClass = dice.attr("class");
    var newClass = "show-" + side;

    dice.removeClass();
    dice.addClass(newClass);

    if (currentClass == newClass) {
        dice.addClass("show-same");
    }
}

function soundEffect() {
    if( soundEnabled ){
        var audio = $("audio")[0];
        audio.pause();
        audio.currentTime = 0;
        audio.play();
    }
}

function blockThisDiceByIndex(index, dice_html){
    
    if( $("#dice" + index).hasClass("blocked") ){
        $("#dice" + index).removeClass("blocked");
        $(dice_html).removeClass("blocked");
    }else{
        if( $("#dices-container .dices-group div.blocked").length < maxDicesBlocked ){
            $("#dice" + index).addClass("blocked");
            $(dice_html).addClass("blocked");
        }else{
            alert( "You may block no more dices. \nMaximum allowed: " + maxDicesBlocked );
        }
        
    }
    
}

function manageDices() {
    var numbers = new Array(numberOfDices);
    for (i = 1; i <= numberOfDices; i++) {
        var currentDice = $("#dice" + i);
        if( currentDice.hasClass("blocked") ){
            numbers[i - 1] = currentPlay[i - 1];
            continue;
        }else{

            var number = randomizeNumber();
            numbers[i - 1] = number;
            if (number == 1) {
                rollDice("front", currentDice);
            } else if (number == 2) {
            rollDice("back", currentDice);
            } else if (number == 3) {
            rollDice("right", currentDice);
            } else if (number == 4) {
            rollDice("left", currentDice);
            } else if (number == 5) {
            rollDice("top", currentDice);
            } else if (number == 6) {
            rollDice("bottom", currentDice);
             }
        }
    }
    soundEffect();
    currentPlay = numbers;
    updateScore();
    setTimeout(showTryResultScreen, 2500);
    $("#dices-container").addClass("disabled");
}

function tryAgain() {
    players_total_score[ currentPlayer ] -= newScore;
    playRound();
}

function playRound() {

    if(firstRound){
        setUpDicesColor();
        firstRound = false;
    }

    $("#dices-container").removeClass("disabled");

    if ($(".player.playing .current-round > ul > li.current").length <= 0) {
        for (i = 0; i < allowedRounds; i++) {
            var roundnumber = i + 1;
            var round = "<li round='" + roundnumber + "' class='round" + roundnumber + "'>" + roundnumber + "</li>";
            $(".player.playing .current-round > ul").append(round);
            $(".player.playing .points").show();
        }
        $(".player.playing .current-round > ul > li:first-child").addClass("current");
    }
    if ($(".player.playing .current-round > ul > li.current > ul").length <= 0) {
        setUpAttempts();
    }

    $(".play-controls").fadeOut(function () {
        $("#dices-container").css({ display: "table" });
    });
    upAttempts();
}

var showTryResultScreen = function showTryResultScreen() {
    $('.continue-screen-look-dices').hide();
    $('.continue-screen-normal').show();

    if (attempts >= allowedAttempts) {
        if (rounds < allowedRounds + allowedRounds) {
            nextPlayerScreen();
        } else {
            $("#dices-container").fadeOut(function () {
                insertDashboardRoundLine();
                setUpEndContainer();
            });
        }
    } else {
        $("#dices-container").fadeOut(function () {
            $(".play-controls > div").hide();
            $(".continue-screen").show();

            var sides = "";
            for (i = 0; i < numberOfDices; i++) {
                var side = "";
                if (currentPlay[i] == 1) {
                    side ="front";
                } else if (currentPlay[i] == 2) {
                    side = "back";
                } else if (currentPlay[i] == 3) {
                    side = "right";
                } else if (currentPlay[i] == 4) {
                    side = "left";
                } else if (currentPlay[i] == 5) {
                    side = "top";
                } else if (currentPlay[i] == 6) {
                    side = "bottom";
                }

                sides += "<figure class='"+side+"' onclick='blockThisDiceByIndex(" + ( i + 1 ) + ", this);'></figure>";
            }
            $(".play-controls .play_preview").html(sides).show();

            $(".play-controls").fadeIn();
        });
    }
};

function setUpDicesColor(){
    var color = "";
    if (currentPlayer == 1) {
        color = color_dices_player1;
    }else{
        color = color_dices_player2;
    }
    console.log(color);

    $(".dice-container").removeClass("black");
    $(".dice-container").removeClass("white");
    $(".dice-container").removeClass("green");
    $(".dice-container").removeClass("red");
    $(".dice-container").removeClass("blue");
    $(".dice-container").removeClass("yellow");

    $(".dice-container").addClass(color);
    
}

function setUpEndContainer() {
    $("#end-container").fadeIn();
}

function upAttempts() {
    attempts++;
    $(".player.playing")
        .find(".attempt" + attempts)
        .addClass("done");
}

function insertDashboardRoundLine() {
    $('#dashboard').fadeIn("fast");
    
    if (currentRoundCount == 2) {
        currentRoundCount = 0;
        currentRound++;
    }
    currentRoundCount++;

    var line = "<li class='initializing'><p class='round'>" + currentRound + "</p><p class='play_points'>" + newPlay + ': ' + newScore + "</p><div class='play_preview'></div></li>"
    if($('#player1.playing').length > 0){
        $(".dashboard.player1 ul").append(line);
    }else{
        $(".dashboard.player2 ul").append(line);        
    }
    for (i = 0; i < numberOfDices; i++) {
        var side = "";
        if (currentPlay[i] == 1) {
            side ="front";
        } else if (currentPlay[i] == 2) {
            side = "back";
        } else if (currentPlay[i] == 3) {
            side = "right";
        } else if (currentPlay[i] == 4) {
            side = "left";
        } else if (currentPlay[i] == 5) {
            side = "top";
        } else if (currentPlay[i] == 6) {
            side = "bottom";
        }

        $(".dashboard ul li.initializing .play_preview").append("<figure class='"+side+"'></figure>");
    }
    if ($('#player1.playing').length > 0) {
        $(".dashboard.player1 ul li.initializing").removeClass("initializing");
    } else {
        $(".dashboard.player2 ul li.initializing").removeClass("initializing");
    }
}

function changeCurrentPlayer(){
    if (currentPlayer == 1) {
        currentPlayer = 2;
    }else{
        currentPlayer = 1;
    }
}

function nextPlayerScreen() {
    changeCurrentPlayer();
    //updateScore();
    updateScoreIndicator();
    insertDashboardRoundLine();

    if ( rounds < ( allowedRounds * 2 ) ) {
        $("#dices-container").fadeOut(function () {
            $(".play-controls > div").hide();
            $(".score-table").show();
            $(".play-controls").fadeIn(function(){
                setUpDicesColor();
            });
        });
        
        $(".player.playing .current-round > ul > li.current > ul").remove();
        $(".player.playing .current-round > ul > li.current").addClass("done");
        $(".player.playing .current-round > ul > li.current.done").addClass(
            "lastPlay"
        );
        $(".player.playing .current-round > ul > li.current.done").removeClass(
            "current"
        );
    } else {

        setUpEndContainer();

    }
}
function goMenu() {
    $("#playing-container").fadeOut();
    $("#end-container").fadeOut();
    $("#player1-first-name").removeClass("disabled");
    $("#player1-last-name").removeClass("disabled");
    $("#player2-first-name").removeClass("disabled");
    $("#player2-last-name").removeClass("disabled");
    $("#player1-first-name").val("");
    $("#player1-last-name").val("");
    $("#player2-first-name").val("");
    $("#player2-last-name").val("");
    $("#submitPlayer1").show();
    $("#submitPlayer2").show();
    $(".play-controls > div").hide();
    $(".player .current-round ul li").remove();
    clearDashboard();
    $("#dashboard").hide();
    $(".dice_color_options.selected").removeClass("selected");
    firstPlayerReady = 0;
    secondPlayerReady = 0;
    $("#start-container").fadeIn();
    players_total_score = {1: 0, 2: 0};
}

function clearDashboard(){
    $("#dashboard li:not(.head)").remove();
}

function setUpEndContainer() {
    if (players_total_score[1] > players_total_score[2]) {
        $("#end-container p #winner").html("Player 1");
    } else if (players_total_score[1] < players_total_score[2]) {
        $("#end-container p #winner").html("Player 2");
    } else {
        $("#end-container p").html("It's a Draw!");
    }

    $("#playing-container").fadeOut(function () {
        $("#end-container").fadeIn();
    });
}

function updateScoreIndicator() {
    var score = $(".player.playing")
        .find(".points-number")
        .text();
    var updatedScore = parseInt(score) + newScore;
    if ($("#player1.playing").length > 0) {
        player1points = updatedScore;
    } else {
        player2points = updatedScore;
    }

    $(".player.playing .points-number").html(updatedScore);
}

function nextPlayer() {
    attempts = 0;
    rounds++;

    if ($("#player1.playing").length <= 0) {
        $("#player2").removeClass("playing");
        $("#player1").addClass("playing");
        $(".player.playing .current-round > ul > li.lastPlay")
            .next()
            .addClass("current");
        $(".player.playing .current-round > ul > li.lastPlay").removeClass(
            "lastPlay"
        );
    } else {
        $("#player1").removeClass("playing");
        $("#player2").addClass("playing");
        $(".player.playing .current-round > ul > li.lastPlay")
            .next()
            .addClass("current");
        $(".player.playing .current-round > ul > li.lastPlay").removeClass(
            "lastPlay"
        );
    }
    
    $("#dices-container .dices-group div").removeClass("blocked");
    playRound();
}


function updateScore() {
    calculateScore();
    $(".interaction-screen")
        .find(".round-score")
        .html(newScore);
}

function calculateScore() {
    
    var this_play_converted = new Array();
    var currentPlaySize = currentPlay.length;
    for (var i = 0; i <= currentPlaySize; i++) {
        this_play_converted.push( dices_faces_numbers[ currentPlay[i] ] );
    }

    $.ajax({
      method: "POST",
      url: "ajax_get_points.php",
      data: {'points': this_play_converted },
      success: function(response){
        
        response = JSON.parse( response );

        newScore = response.score;
        newPlay = response.play;
        $(".interaction-screen")
            .find(".round-score")
            .html( newScore );


        players_total_score[ currentPlayer ] += response.score;

        $(".interaction-screen")
            .find(".round-total-score")
            .html( players_total_score[ currentPlayer ] );

      }

    });

}


function selectColor(clicked_color){
    manageSiblingSelectedColor(clicked_color);
    
    if ($(clicked_color).hasClass("selected")){
        $(clicked_color).removeClass("selected");        
    }else{
        var color = checkColor(clicked_color);

        switch (color) {
            case "black":
                $(".dice_color_options li.color_black").not(clicked_color).addClass('disabled');
                break;
            case "white":
                $(".dice_color_options li.color_white").not(clicked_color).addClass('disabled');
                break;
            case "red":
                $(".dice_color_options li.color_red").not(clicked_color).addClass('disabled');
                break;
            case "green":
                $(".dice_color_options li.color_green").not(clicked_color).addClass('disabled');
                break;
            case "blue":
                $(".dice_color_options li.color_blue").not(clicked_color).addClass('disabled');
                break;
            case "yellow":
                $(".dice_color_options li.color_yellow").not(clicked_color).addClass('disabled');
                break;
            default:
            // code block
        }

        $(clicked_color).addClass("selected");
    }
        
}

function manageSiblingSelectedColor(clicked_color){
        var siblings = $(clicked_color).siblings();
        
        for (i=0; i< siblings.length; i++){
            if ($(siblings[i]).hasClass("selected")){
                var selected = $(siblings[i]);
                var color = checkColor($(siblings[i]));

                switch (color) {
                    case "black":
                        $(".dice_color_options li.color_black").not(selected).removeClass('disabled');
                        break;
                    case "white":
                        $(".dice_color_options li.color_white").not(selected).removeClass('disabled');
                        break;
                    case "red":
                        $(".dice_color_options li.color_red").not(selected).removeClass('disabled');
                        break;
                    case "green":
                        $(".dice_color_options li.color_green").not(selected).removeClass('disabled');
                        break;
                    case "blue":
                        $(".dice_color_options li.color_blue").not(selected).removeClass('disabled');
                        break;
                    case "yellow":
                        $(".dice_color_options li.color_yellow").not(selected).removeClass('disabled');
                        break;
                    default:
                    // code block
                }

                $(selected).removeClass("selected");
            }
        }
        
    }

    function checkColor(clicked_color){
        if ($(clicked_color).hasClass('color_black')) {
            return "black";
        } else if ($(clicked_color).hasClass('color_white')) {
            return "white";
        } else if ($(clicked_color).hasClass('color_green')) {
            return "green";
        } else if ($(clicked_color).hasClass('color_yellow')) {
            return "yellow";
        } else if ($(clicked_color).hasClass('color_red')) {
            return "red";
        } else if ($(clicked_color).hasClass('color_blue')) {
            return "blue";
        }
    }

    function getSelectedColor(player){
        if ($('#form-player' + player + ' .dice_color_options li.selected').length <= 0){
            return "none";
        }
        return checkColor($('#form-player' + player + ' .dice_color_options li.selected'));
    }

function lookDicesSceeen(){
    $('.continue-screen-normal').fadeOut(function(){
        $('.continue-screen-look-dices').fadeIn();
    });
}


function manageHelpPopUp(){
    if( $(".popup-container").is(":visible") ){
        $(".popup-container").slideUp();
    }else{
        $(".popup-container").slideDown();
    }
}

function manageSound(){

    if( soundEnabled ){
        $(".volume-icon").addClass("volume-icon--muted").removeClass("volume-icon--enabled");
        soundEnabled = false;
    }else{
        $(".volume-icon").addClass("volume-icon--enabled").removeClass("volume-icon--muted");
        soundEnabled = true;
    }

}

function selectDifficulty( difficulty ){

    $("#difficulty label.selected").removeClass("selected");

    switch( difficulty ){
        case 1:
            $("#difficulty label[for='easy']").addClass("selected");
            setUpVariables(3, 3, 4, 5);
            break;
        case 2:
            $("#difficulty label[for='medium']").addClass("selected");
            setUpVariables(3, 3, 3, 5);
            break;
        case 3:
            $("#difficulty label[for='hard']").addClass("selected");
            setUpVariables(2, 3, 0, 5);
            break;

    }

}

/*$(document).ready(function(){
    $("body:not(div.info-icon)").on("click", function(){
        console.log("AQUI!");
        if( $(".popup-container").is(":visible") ){
            $(".popup-container").slideUp();
        }
    });
});*/